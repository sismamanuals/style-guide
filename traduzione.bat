Rem make gettext

sphinx-intl update -p build/gettext -l it -l en

sphinx-intl update -p build/gettext -l it -l es

sphinx-intl update -p build/gettext -l it -l fr

sphinx-intl update -p build/gettext -l it -l de

sphinx-intl update -p build/gettext -l it -l ru

sphinx-intl update -p build/gettext -l it -l tr

sphinx-intl update -p build/gettext -l it -l pt

sphinx-intl update -p build/gettext -l it -l cs
