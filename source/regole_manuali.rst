.. _regole_manuali:

###########################
REGOLE DI STILE PER MANUALI
###########################
In questo capitolo si forniranno alcune indicazioni di carattere generale per le regole da utilizzare all'interno dei manuali per la loro struttura.

************************************
FORMULAZIONE SEMPLICE PER ISTRUZIONI
************************************
Le istruzioni devono essere descritte come segue:

* in ordine sequenziale, preferibilmente una sotto l'altra;
* un'istruzione per frase. Possono essere fornite due istruzioni per frase solo se devono essere eseguite contemporaneamente.

Ogni istruzione è normalmente composta da quattro parti:

#. il numero dell'istruzione o altri mezzi tipografici per indicare l'ordine delle istruzioni (iniziare la numerazione con 1, non con 0);
#. la formulazione dell'istruzione;
#. la spiegazione, ovvero lo scopo o la ragione dell'istruzione;
#. l'effetto dell'istruzione per verificare il funzionamento della funzione richiesta:

   a. descrivere il risultato di un'istruzione, ove necessario;
   #. descrivere lo stato della macchina o del processo dopo il completamento di un'istruzione, dove necessario;
   #. con diversi risultati, spiegando le situazioni che ciascuna istruzione comporta;
   #. descrivendo la fine di un'istruzione e anche come può essere rilevata una conclusione anomala.

Bisogna rispettare le seguenti regole:

* usare l'imperativo;
* evitare parole che possono cambiare la cronologia come "dopo", "prima";
* evitare parole che indeboliscono l'istruzione;
* evitare parole negate;
* mantenere le frasi descrittive il più breve possibile;
* affrontare un solo argomento per paragrafo;
* suddividere le informazioni in argomenti secondari in paragrafi separati, se un paragrafo è insufficiente;
* usare sempre la stessa parola per una parte o un'azione particolare;
* impartire istruzioni il più specifiche possibile;
* usare la stessa terminologia ovunque.

*********************************
RACCOMANDAZIONI PER LE ISTRUZIONI
*********************************
.. |br| raw:: html

    <br>

.. csv-table:: Instructions
   :header-rows: 1
   :widths: 30, 35, 35
   :file: instruction_table.csv

.. csv-table:: Sentences
   :header-rows: 1
   :widths: 30, 35, 35
   :file: sentences_table.csv

.. csv-table:: Words
   :header-rows: 1
   :widths: 30, 35, 35
   :file: words_table.csv

.. csv-table:: Verbs
   :header-rows: 1
   :widths: 30, 35, 35
   :file: verbs_table.csv

.. csv-table:: Writing
   :header-rows: 1
   :widths: 30, 35, 35
   :file: writing_table.csv


***************************
ENFATIZZARE LE INFORMAZIONI
***************************
In ogni manuale di istruzioni, dovrebbero essere usati metodi per indicare informazioni di particolare interesse
(per esempio: precauzioni, avvertenze e istruzioni di sicurezza, ecc...).

L'attenzione dovrebbe essere focalizzata sull'enfasi informazioni contenute nei manuali di istruzioni in modo sufficientemente evidente. Dovrebbero essere scelti metodi distintivi (ad esempio posizionare le informazioni proprio all'inizio e/o sotto un titolo che attira attenzione): tali metodi devono essere di uso comune ed applicati coerentemente.

Un metodo convenzionale è l'uso di lettere in grassetto e corsivo, l'uso di linee e cornici o l'uso di
colori. La metodologia di dovrebbe essere descritta nella prefazione in modo che gli utenti possano avere chiaro il significato delle enfatizzazioni. Per specifiche di dettaglio vedere :numref:`regole_enfasi`.

.. NOTE::

   |notice| Evitare di enfatizzare troppo. Troppi elementi enfatizzati distraggono il lettore dalla comprensione delle informazioni utili.

*******
ELENCHI
*******
Per gli elenchi utilizzare le seguenti regole:

* uso di un *elenco puntato* per descrivere una serie di oggetti o istruzioni non legate tra loro da una sequenza temporale (per esempio: una descrizione di una lista di comandi) (vedere :numref:`elenco_puntato`);
* uso di un *elenco numerato* per descrivere una serie di oggetti o istruzioni legate tra loro da una sequenza temporale (per esempio: una descrizione di una lista di operazioni da eseguirsi in sequenza) (vedere :numref:`elenco_numerato`);
* per l'indentazione di un *elenco numerato* si deve utilizzare la numerazione letterale.

********
IMMAGINI
********
Quando viene descritta una sequenza di operazioni, il testo e le immagini devono seguire la sequenza operativa. Le immagini devono essere collocate il più vicino possibile al testo a cui si riferiscono, in modo che possano essere visualizzate accanto al testo pertinente.

Laddove le immagini necessitino di testi esplicativi, il testo deve essere posizionato accanto all'immagine. In alternativa è obbligatorio inserire un *riferimento incrociato* all'interno del testo per richiamare l'immagine.

Una sequenza di immagini deve essere logica e comprensibile. Il testo correlato e le immagini devono essere entrambe visualizzabili contemporaneamente, ciascuna a supporto dell'altra per migliorare la comprensibilità.

Le immagini devono essere adatte al pubblico a cui sono indirizzate. Le immagini devono essere create in modo da focalizzare l'attenzione su dettagli importanti ed essere autoesplicative.

La qualità di stampa o la risoluzione dello schermo delle immagini deve favorire una rapida comprensione. Le risoluzioni inferiori a 72 dpi possono ostacolare in modo significativo la chiarezza. Sono preferibili risoluzioni pari o superiori a 300 dpi.

CONTENUTO INFORMATIVO
=====================
Il sovraccarico di informazioni nelle immagini deve essere evitato. Le immagini devono normalmente fornire
solo informazioni rilevanti la descrizione della funzione correlata. Immagini o parti dettagliate di
queste dovrebbero essere ripetute nelle parti pertinenti delle informazioni per l'uso, se necessario.

DIDASCALIE
==========
Le immagini devono essere identificate e integrate con didascalie. Ci deve essere una chiara
relazione tra le immagini e le didascalie. Immagini e didascalie correlate devono trovarsi in posizioni simili (ad es. illustrazione sopra la didascalia o didascalia sopra
illustrazione, non mista). Le didascalie devono essere numerate per un chiaro riferimento.

*******
TABELLE
*******
Le tabelle devono essere definite in modo chiaro, informativo e secondo una progettazione coerente. Le tabelle devono essere posizionate accanto al testo pertinente, ad eccezione di quelle tabelle che fungono da riferimento generale, che possono essere collocate negli allegati.

******
COLORI
******
Se il pubblico include persone con una visione carente di colore, è necessario utilizzare i colori solo come mezzo supplementare per attirare l'attenzione su particolari sezioni di informazioni.

Le informazioni mostrate con colori contrastanti dovrebbero rimanere distinte, se è previsto che il pubblico di destinazione possa stamparle su una stampante monocromatica.

*****
ICONE
*****
Le icone devono rappresentare in modo univoco oggetti o funzioni. Icone diverse non devono mai essere usate per
lo stesso oggetto o funzione. Allo stesso modo, diversi oggetti o funzioni non devono essere rappresentati
dalla stessa icona. Se un'icona rappresenta cambiamenti nello stato di un elemento, il collegamento deve
essere rappresentato in modo coerente.
