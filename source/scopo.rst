.. _scopo:

#####
SCOPO
#####
Scopo del documento è di fornire indicazioni di carattere generale per le regole da utilizzare all'interno dei manuali, sia per la loro struttura che per il tipo di linguaggio utilizzato.

*********************
RIFERIMENTI NORMATIVI
*********************

* Norma tecnica UNI EN ISO 20607:2019

   Sicurezza del macchinario - Manuale di istruzioni - Principi generali di redazione.

* Norma tecnica IEC 82079:2019

   Preparation of information for use (instructions for use) of products Principles and general requirements.
