.. SISMA Regole di Stile master file, created by
   sphinx-quickstart on Tue Jan  7 13:21:25 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

#####################
SISMA Regole di Stile
#####################

.. toctree::
   :maxdepth: 3
   :numbered:

   scopo
   regole_manuali
   regole_enfasi
   reStructuredText_commands
