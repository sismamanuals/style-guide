.. _regole_enfasi:

########################
REGOLE DI ENFATIZZAZIONE
########################
Lo scopo del presente capitolo è di fornire delle indicazioni generali sulle informazioni da enfatizzare.

.. _avviso_sicurezza:

******************************
MESSAGGI DI AVVISO E SICUREZZA
******************************
I messaggi di avviso e sicurezza devono essere inseriti accompagnati da un pittogramma, in modo da rendere immediatamente comprensibile il livello di pericolo collegato.

Le descrizioni precedute da un pittogramma di colore rosso/giallo/arancione/azzurro contengono informazioni/prescrizioni molto importanti, anche per quanto riguarda la sicurezza, sia dell'operatore che del macchinario.

Di seguito si riportano le diciture con spiegazione di dettaglio del loro significato.

.. NOTE::

   |notice| Viene utilizzato per affrontare le pratiche non legate a lesioni fisiche.

.. WARNING::

   |warning| Indica una situazione di rischio potenziale che, se non prevista, potrebbe causare danni di minore o modesta entità.

.. CAUTION::

   |caution| Indica una situazione di rischio potenziale che, se non evitata, può causare morte o danno grave.

.. DANGER::

   |danger| Indica una situazione di rischio imminente che, se non evitata, causa morte o danno grave.

.. _comandi:

*******
COMANDI
*******
Evidenziare i comandi che l'operatore deve utilizzare per compiere le azioni descritte.

Enfatizzare sia i pulsanti fisici (per esempio: pulsante di avvio sul pannello comandi) che i pulsanti immateriali (per esempio: comando di avvio presente nel software di controllo della macchina).

Differenziare il metodo, nel caso di comandi software, tra i pulsanti della tastiera (per esempio: premere il tasto :kbd:`Ctrl` + :kbd:`C`) e i pulsanti o menù a video (per esempio: selezionare il comando :guilabel:`File -> Apri`).

Nella descrizione di comandi software utilizzare lo stesso metodo anche per distinguere i nomi delle eventuali finestre o tab presenti dai comandi all'interno delle stesse (per esempio: selezionare la finestra :guilabel:`IMPOSTAZIONI APPLICAZIONI`, selezionare il menù :guilabel:`Lingua` e cliccare sul pulsante :kbd:`APPLICA`).

.. |abilita_utente| image:: _static/impostazioni_utenti_abilita.png
   :width: 1 cm
   :align: middle

Per descrivere un pulsante rappresentato da un simbolo grafico è preferibile riportare direttamente il simbolo (per esempio: Il pulsante |abilita_utente| permette di abilitare una funzione).

.. _altre_indicazioni:

*****************
ALTRE INDICAZIONI
*****************
Altri elementi da evidenziare possono essere:

* Nomi variabili (per esempio: ``MarkingReportCSVFileAppender``);
* Estensioni di file (per esempio: **.txt**);
* Percorsi di file (per esempio: `C:\Sisma\Databases`);
* Nomi specifici di Prodotti e/o Softwares (per esempio: **Microsoft SqlServer**);
* Parti di codice di un linguaggio di programmazione, evidenziando i comandi relativi, come:

   .. code-block:: XML

      <!-- Marking Report Logger -->
      <logger name="SLC.Log.Report.ReportUtils" additivity="false">
         <level value="ALL" /> <appender-ref ref="MarkingReportCSVFileAppender" />
         <!--<appender-ref ref="MarkingReportSQLiteAppender" />-->
         <!--<appender-ref ref="MarkingReportSqlServerAppender" />-->
      </logger>

* Indirizzi internet (per esempio <https://www.sisma.com/>).

.. NOTE::

   |notice| Per la sintassi relativa alle modalità grafiche all'interno del linguaggio `reStructuredText` riferirsi alla :numref:`reStructuredText_commands`.
